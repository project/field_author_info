<?php

namespace Drupal\field_author_info\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_author_info_text' widget.
 *
 * @FieldWidget(
 *   id = "author_info",
 *   module = "field_author_info",
 *   label = @Translation("Author Information"),
 *   field_types = {
 *     "author_info"
 *   }
 * )
 */
class AuthorInfoWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['name'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#default_value' => $items->offsetExists($delta) ? $items->get($delta)->name : '',
      '#size' => 60,
      '#maxlength' => 256,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $element['first_surname'] = [
      '#title' => $this->t('Fist surname'),
      '#type' => 'textfield',
      '#default_value' => $items->offsetExists($delta) ? $items->get($delta)->first_surname : '',
      '#size' => 60,
      '#maxlength' => 256,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $element['second_surname'] = [
      '#title' => $this->t('Second surname'),
      '#type' => 'textfield',
      '#default_value' => $items->offsetExists($delta) ? $items->get($delta)->second_surname : '',
      '#size' => 60,
      '#maxlength' => 256,
      '#required' => FALSE,
    ];

    $element['organization'] = [
      '#title' => $this->t('Organization'),
      '#type' => 'textfield',
      '#default_value' => $items->offsetExists($delta) ? $items->get($delta)->organization : '',
      '#size' => 60,
      '#maxlength' => 256,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $field_name = $this->fieldDefinition->getName();
    $element['main'] = [
      '#title' => $this->t('Main author'),
      '#type' => 'radio',
      '#default_value' => $items->offsetExists($delta) ? $items->get($delta)->main : NULL,
      '#return_value' => $delta,
      '#parents' => [
        $field_name . '__main',
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $elements = parent::formMultipleElements($items, $form, $form_state);
    // Delete the last element unless the field is empty.
    if (!$items->isEmpty()) {
      $max = $elements['#max_delta'];
      unset($elements[$max]);
      $elements['#max_delta'] = $max - 1;
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    $input = $form_state->getUserInput();
    $field_name = $this->fieldDefinition->getName();

    if (!empty($input[$field_name . '__main'])) {
      $active_delta = intval($input[$field_name . '__main']);

      foreach ($values as $key => &$value) {
        if (is_array($value)) {
          if ($value['_original_delta'] === $active_delta) {
            $value['main'] = $active_delta;
          }
          else {
            $value['main'] = NULL;
          }
        }
      }
    }
    else {
      // If not selected value, set default value to first item.
      foreach ($values as $key => &$value) {
        if (is_array($value)) {
          // By default is using first element _original_delta value.
          if ($value['_original_delta'] === 0) {
            $value['main'] = $key;
            $form_state->setValue($field_name . '__main', $key);
          }
          else {
            $value['main'] = NULL;
          }
        }
      }
    }

    return parent::massageFormValues($values, $form, $form_state);
  }

}
