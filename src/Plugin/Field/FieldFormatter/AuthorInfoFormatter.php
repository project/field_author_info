<?php

namespace Drupal\field_author_info\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'author_info' formatter.
 *
 * @FieldFormatter(
 *   id = "author_info",
 *   module = "field_author_info",
 *   label = @Translation("Author Information"),
 *   field_types = {
 *     "author_info"
 *   }
 * )
 */
class AuthorInfoFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        'name' => [
          '#markup' => $item->name,
        ],
        'first_surname' => [
          '#markup' => ' ' . $item->first_surname,
        ],
        'second_surname' => [
          '#markup' => ' ' . $item->second_surname,
        ],
        'organization' => [
          '#markup' => ' - ' . $item->organization,
        ],
      ];
    }

    return $elements;
  }

}
