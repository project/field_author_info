<?php

namespace Drupal\field_author_info\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'author_info' formatter.
 *
 * @FieldFormatter(
 *   id = "author_coauthors_info",
 *   module = "field_author_info",
 *   label = @Translation("Author and coauthors Information"),
 *   field_types = {
 *     "author_info"
 *   }
 * )
 */
class AuthorInfoCoauthorsFormmater extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $author = [];
    $coauthors = [];

    foreach ($items as $delta => $item) {

      if (!is_null($item->main)) {
        $author = [
          'name' => $item->name,
          'first_surname' => $item->first_surname,
          'second_surname' => $item->second_surname,
          'organization' => $item->organization,
        ];
      }
      else {
        $coauthors[] = [
          'name' => $item->name,
          'first_surname' => $item->first_surname,
          'second_surname' => $item->second_surname,
          'organization' => $item->organization,
        ];
      }

    }

    $elements = [
      '#theme' => 'author_coauthors_formatter',
      '#author' => $author,
      '#coauthors' => $coauthors,
    ];

    return $elements;
  }

}
