<?php

namespace Drupal\field_author_info\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'field_author_info' field type.
 *
 * @FieldType(
 *   id = "author_info",
 *   label = @Translation("Author Information"),
 *   module = "field_author_info",
 *   description = @Translation("Author Information."),
 *   default_widget = "author_info",
 *   default_formatter = "author_info"
 * )
 */
class AuthorInfo extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'name' => [
          'type' => 'varchar',
          'length' => '255',
        ],
        'first_surname' => [
          'type' => 'varchar',
          'length' => '255',
        ],
        'second_surname' => [
          'type' => 'varchar',
          'length' => '255',
        ],
        'organization' => [
          'type' => 'varchar',
          'length' => '255',
        ],
        'main' => [
          'type' => 'int',
          'length' => 'tiny',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('name')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['name'] = DataDefinition::create('string')
      ->setLabel(t('Name'));
    $properties['first_surname'] = DataDefinition::create('string')
      ->setLabel(t('First surname'));
    $properties['second_surname'] = DataDefinition::create('string')
      ->setLabel(t('Second surname'));
    $properties['organization'] = DataDefinition::create('string')
      ->setLabel(t('Organization'));
    $properties['main'] = DataDefinition::create('integer')
      ->setLabel(t('Main author'));

    return $properties;
  }

}
